const ports = {
    SERVER_PORT: 3444
    , SERVER_DEVELOPER_PORT: undefined
};

module.exports = ports;